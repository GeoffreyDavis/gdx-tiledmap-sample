package com.geoffreydavis.gdxsample.tiledmap.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.geoffreydavis.gdxsample.tiledmap.GdxTiledMapSample;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new GdxTiledMapSample(), config);
	}
}
