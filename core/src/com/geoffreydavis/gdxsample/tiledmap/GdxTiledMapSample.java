package com.geoffreydavis.gdxsample.tiledmap;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Geoffrey Davis
 */
public class GdxTiledMapSample extends ApplicationAdapter {
    /**
     * Clamps the {@link Camera} position.
     */
    private void clampCamera() {
        // Retrieve the world size.
        final int worldWidth = mTiledMap.getProperties().get("width", Integer.class);
        final int worldHeight = mTiledMap.getProperties().get("height", Integer.class);

        // Retrieve the camera we're going to use for our calculations.
        mCamera.zoom = MathUtils.clamp(mCamera.zoom, 0.1f, 100f / mCamera.viewportWidth);

        // Calculate the effective size of the viewport.
        float effectiveViewportWidth = mCamera.viewportWidth * mCamera.zoom;
        float effectiveViewportHeight = mCamera.viewportHeight * mCamera.zoom;

        mCamera.position.x = MathUtils.clamp(mCamera.position.x, effectiveViewportWidth / 2f, worldWidth - effectiveViewportWidth / 2f);
        mCamera.position.y = MathUtils.clamp(mCamera.position.y, effectiveViewportHeight / 2f, worldHeight - effectiveViewportHeight / 2f);
        mCamera.update();
    }

    /**
     * Do camera setup
     */
    private void configureCamera() {
        // The width and height of the world, given in tiles not pixels.
        final int worldWidth = mTiledMap.getProperties().get("width", Integer.class);
        final int worldHeight = mTiledMap.getProperties().get("height", Integer.class);

        // Create the new camera object.
        mCamera = new OrthographicCamera();

        // Position the camera in the world.
        mCamera.setToOrtho(false, 20f, 12.5f);
        mCamera.position.set(worldWidth / 2.0f, worldHeight / 2.0f, 0.0f);
        mCamera.update();
    }

    /**
     * Do imput processor setup
     */
    private void configureInputProcessor() {
        Gdx.input.setInputProcessor(new InputAdapter() {
            private Vector3 mTouchDown = new Vector3();

            @Override
            public boolean touchDown(int x, int y, int pointer, int button) {
                return mTouchDown.set(x, y, 0) != null;
            }

            @Override
            public boolean touchDragged(int x, int y, int pointer) {
                float deltaX = (mTouchDown.x - x) / Gdx.graphics.getWidth();
                float deltaY = (y - mTouchDown.y) / Gdx.graphics.getHeight();
                mTouchDown.set(x, y, 0);
                final float unitScale = mTiledMapRenderer.getUnitScale();
                mCamera.position.add(deltaX / unitScale, deltaY / unitScale, 0f);
                mCamera.update();
                clampCamera();
                return true;
            }
        });
    }

    /**
     * Do TMX tiled map setup
     */
    private void configureTiledMap() {
        mTiledMap = new TmxMapLoader().load("map/tiledmap.tmx");
    }

    /**
     * Do tiled map renderer setup
     */
    private void configureTiledMapRenderer() {
        // Some properties we need from the tiled map.
        final int tileWidth = mTiledMap.getProperties().get("tilewidth", Integer.class);
        final int tileHeight = mTiledMap.getProperties().get("tileheight", Integer.class);

        // Calculate the tile "size" from its width and height.
        final int tileSize = Math.min(tileWidth, tileHeight);

        // The number of pixels in the smaller size of the tile determines the unit scale.
        mTiledMapRenderer = new OrthogonalTiledMapRenderer(mTiledMap, 1f / tileSize);
    }

    @Override
    public void create() {
        configureTiledMap();
        configureTiledMapRenderer();
        configureCamera();
        configureInputProcessor();
    }

    @Override
    public void render() {
        // Clear the screen before we draw.
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Render the tiled map.
        mTiledMapRenderer.setView(mCamera);
        mTiledMapRenderer.render();
    }

    @Override
    public void dispose() {
        mTiledMapRenderer.dispose();
        mTiledMap.dispose();
    }

    /**
     * The {@link OrthographicCamera} object.
     */
    private OrthographicCamera mCamera;

    /**
     * The TMX tiled map object.
     */
    private TiledMap mTiledMap;

    /**
     * The renderer object.
     */
    private OrthogonalTiledMapRenderer mTiledMapRenderer;
}
